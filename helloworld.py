'''
Author:  Scott Field
Version: 1.0
Date: 4/18/2022
Name: helloworld
Purpose: Create a simple hello world flask application
'''

from flask import Flask
app = Flask(__name__)
 
@app.route("/")
def hello():
    return "Hello World!"
 
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')